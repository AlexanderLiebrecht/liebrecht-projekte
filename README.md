## Willkommen in meinem Repository für eigene Projekte ##
Hier erfahrt ihr etwas zu meinen CMS-Projekten, Blogs und Foren, die ich betreibe. Ich möchte einfach auch auf BitBucket mein Wissen und Erfahrungen zeigen und anbieten. 

Ich selbst bin ein bloggender Webmaster im 10ten Jahr und so lange befasse ich mich schon damit. In den letzten 10 Jahren sammelte ich zahlreiche Erfahrungen als bloggender Webmaster und habe etliches getestet und gesichtet. Es machte Spass und diese CMS-Arbeit kam zum Beispiel seit 2013 zu mir.

### Seit 2013 lernte ich all diese besten CMS kennen

Es war sehr toll und nützlich. Ich konnte etliche OpenSource/Flat File CMS testen und betreibe heute allerhand davon. Es macht mir möglich, viele wertvolle CMS-Erfahrungen zu sammeln, noch mehr zu lernen und eigenes Wissen anzuhäufen. Auch dient es der Horizont-Erweiterung, sodass es immer zum Lernen kommen kann. 

Infos und Details zu meinerseits eingesetzten OpenSource CMS verstreue ich im ganzen Portfolio und oft auch in diesem Forum unter https://rostockerblogger.de Das ist eines meiner Lieblings-CMS/Blogger-Foren, welches täglich bepostet wird. Dort landen allerhand Berichte und ich habe im Forum noch [Blog](https://internetblogger.de), Artikel, Galerie. Linkverzeichnis und [einen nützlichen Wiki](https://webmasterwelten.de/wiki).

## Was gibt es in diesem Bitbucket-Projekt

Dieses Projekt hat ein Ticketsystem und den Wiki und ich versuche, hier nach und nach, meine Inhalte zu platzieren. Im Ticketsystem zum Beispiel stelle ich auch selbst Fragen, um sie für euch zu beantworten. Den Wiki nutze ich dafür, um entweder zu bloggen oder CMS-Erfahrungen preiszugeben. 

![confluence-server-projekt-im-frontend.png](https://bitbucket.org/repo/4kRrEo/images/3601582002-confluence-server-projekt-im-frontend.png)

Es macht mir viel Spass, dieses Projekt zu beposten und zudem kenne ich die Produkte aus dem Hause Atlassian schon etwas. Ich arbeite dauerhaft mit Confluence/Bitbucket-Server und Jira, sodass etliche eigenen Erfahrungen gesammelt werden können. Das gebe ich an meine Stammleserschaft weiter und lerne auch mit den Lesern, falls Fragen gestellt werden.

Hier im Projekt wünsche ich euch viel Spass beim Stöbern und vielleicht fragt ihr mich auch mal etwas im Issue-Tracker :smile:

## CMS-Service-Shop + Community

Unter https://yaf-forum.de ist mein CMS-Onlineshop entstanden und daran ist eine WebmasterBlogger/CMS-Community angeschlossen. Du kannst gerne jederzeit zu
uns stossen und mit dabei sein. In der Community poste ich zur Zeit am meisten, weil es auch so braucht. Ohne stetige Forum-Inhalte gibt es keine
Weiterentwicklung. 

